﻿using Client.FormIhm.ServiceReferencePim;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Windows.Forms;

namespace Client.FormIhm
{
    public partial class PIM : Form
    {
        ServiceReferencePim.ServicePimClient proxy = null;
        private BagageDefinition selectBag;
        private List<BagageDefinition> listBag = new List<BagageDefinition>();
        private PimState state = PimState.Deconnecter;
        private static FormIhm.Select dialog;
        private PimState State
        {
            get { return this.state; }
            set { OnPimStateChanged(value); }
        }

        public event PimStateEventHandler PimStateChanged;
        public delegate void PimStateEventHandler(object sender, PimState state);

        private void OnPimStateChanged(PimState newState)
        {
            if (newState != this.state)
            {
                this.state = newState;
                if (this.PimStateChanged != null)
                {
                    PimStateChanged(this, this.state);
                }
            }
        }

        void PIM_PimStateChanged(object sender, PimState state)
        {
            switch (State)
            {
                case PimState.AffichageBagage:
                    AffichageBagage();
                    break;
                case PimState.AttenteBagage:
                    AttenteBagage();
                    break;
                case PimState.CreationBagage:
                    CreationBagage();
                    break;
                case PimState.SelectionBagage:
                    SelectionBagage();
                    break;
                default:
                    Deconnecter();
                    break;
            }
        }


        public PIM()
        {
            InitializeComponent();
            proxy = new ServiceReferencePim.ServicePimClient();
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);

            reinitialiserToolStripMenuItem.Click += new EventHandler(this.ReinitialiserToolStripMenuItem_Click);
            this.PimStateChanged += PIM_PimStateChanged;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Client.FormIhm.ServiceReferencePim.BagageDefinition bag = null;
            try
            {
                bag = proxy.GetBagageByCodeIata(this.tbIata.Text);

                //Si le bagage existe, l'afficher
                if (bag != null)
                {
                    selectBag = bag;
                    OnPimStateChanged(PimState.AffichageBagage);
                }
                //Sinon le créer
                else
                {
                    OnPimStateChanged(PimState.CreationBagage);
                }
            }
            catch (FaultException<MultipleBagagesFault> excp)
            {
                //Il y a plusieurs bagages avec le même code iata
                listBag.Clear();
                listBag.AddRange(excp.Detail.ListBagages);                
                OnPimStateChanged(PimState.SelectionBagage);
            }
            catch (FaultException excp)
            {
                listBag.Clear();
                this.toolStripStatusLabel1.Text += "Une erreur s'est produite dans le traitement de votre demande";
                this.toolStripStatusLabel1.Text += "\tCode: " + excp.Code.Name;
                this.toolStripStatusLabel1.Text += "\tReason: " + excp.Reason;

            }
            catch (CommunicationException excp)
            {
                listBag.Clear();
                this.toolStripStatusLabel1.Text += "Une erreur de communication c'est produite dans le traitement de votre demande";
                this.toolStripStatusLabel1.Text += "\tType: " + excp.GetType().ToString();
                this.toolStripStatusLabel1.Text += "\tMessage: " + excp.Message;
            }
            catch (Exception excp)
            {
                listBag.Clear();
                this.toolStripStatusLabel1.Text += "Une erreur s'est produite dans le traitement de votre demande";
                this.toolStripStatusLabel1.Text += "\tType: " + excp.GetType().ToString();
                this.toolStripStatusLabel1.Text += "\tMessage: " + excp.Message;
            }
        }

        private void BtSave_Click(object sender, EventArgs e)
        {
            
            BagageDefinition bag = new BagageDefinition();
            bag.CodeIata = this.tbIata.Text;
            bag.Compagnie = this.tbCompagnie.Text;
            bag.ClasseBagage = this.tbClBagage.Text;
            bag.EnContinuation = this.cbContinuation.Checked;
            bag.Rush = this.cbRush.Checked;
            bag.Prioritaire = this.cbPrioritaire.Checked;
            bag.Itineraire = this.tbItineraire.Text;
            bag.Ligne = this.tbLigne.Text;
            try
            {
                if (proxy.CheckCompagnie(bag.Compagnie) && proxy.CheckIataLength(bag.CodeIata) && proxy.CheckIataExist(bag.CodeIata))
                {
                    this.tbIata.Enabled = false;
                    proxy.CreateBagage(bag);
                    this.toolStripStatusLabel1.Text = "Succès";
                }
            }
            catch(FaultException<IataFormatException> excp)
            {
                this.toolStripStatusLabel1.Text = excp.Detail.Message;
                this.tbIata.Enabled = true;
            }
            catch (FaultException<IataExistException> excp)
            {
                this.toolStripStatusLabel1.Text = excp.Detail.Message;
                OnPimStateChanged(PimState.AttenteBagage);
            }
            catch (FaultException<CompagnieFormatException> excp)
            {
                this.toolStripStatusLabel1.Text = excp.Detail.Message;
            }
            catch (FaultException excp)
            {
                this.toolStripStatusLabel1.Text = "Une erreur s'est produite dans le traitement de votre demande";
            }

        }
        /*
         * Affichage du bagage récupéré
         */
        private void AffichageBagage()
        {
            this.tbIata.Text = selectBag.CodeIata;
            this.tbIata.Enabled = false;
            this.tbCompagnie.Text = selectBag.Compagnie;
            this.tbCompagnie.Enabled = false;
            this.tbLigne.Text = selectBag.Ligne;
            this.tbLigne.Enabled = false;
            this.tbDate.Text = selectBag.JourExploitation.ToString();
            this.tbDate.Enabled = false;
            this.cbContinuation.Enabled = false;
            this.cbRush.Enabled = false;
            if (selectBag.EnContinuation)
            {
                this.cbContinuation.CheckState = CheckState.Checked;
            }
            if (selectBag.Rush)
            {
                this.cbRush.CheckState = CheckState.Checked;
            }
            if (selectBag.Prioritaire)
            {
                this.cbPrioritaire.CheckState = CheckState.Checked;
            }
            this.cbPrioritaire.Enabled = false;
            this.tbItineraire.Text = selectBag.Itineraire;
            this.tbItineraire.Enabled = false;
            this.tbClBagage.Text = selectBag.ClasseBagage;
            this.tbClBagage.Enabled = false;
            this.btSave.Enabled = false;
            this.btSave.Visible = false;
        }

        /*
         * Sélection d'un bagage lorsque plusieurs correspondances au code iata sont retournées
         * Ouvre une windows form pour choisir le bagage 
         */
        private void SelectionBagage()
        {
            dialog = new FormIhm.Select();
            dialog.FillList(listBag);
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                selectBag = dialog.bagageResult;
                OnPimStateChanged(PimState.AffichageBagage);
            }
            else
            {
                OnPimStateChanged(PimState.AttenteBagage);
            }
        }

        private void CreationBagage()
        {
            this.tbIata.Enabled = false;
            this.tbCompagnie.Enabled = true;
            this.tbLigne.Enabled = true;
            this.tbDate.Enabled = false;
            this.cbContinuation.Enabled = true;
            this.cbRush.Enabled = true;
            this.cbPrioritaire.Enabled = true;
            this.tbItineraire.Enabled = true;
            this.tbClBagage.Enabled = true;
            this.btSave.Enabled = true;
            this.btSave.Visible = true;
        }

        private void AttenteBagage()
        {
            this.tbIata.Text = "";
            this.tbIata.Enabled = true;
            this.tbCompagnie.Text = "";
            this.tbCompagnie.Enabled = false;
            this.tbLigne.Text = "";
            this.tbLigne.Enabled = false;
            this.tbDate.Text = "";
            this.tbDate.Enabled = false;
            this.cbContinuation.CheckState = CheckState.Unchecked;
            this.cbContinuation.Enabled = false;
            this.cbRush.CheckState = CheckState.Unchecked;
            this.cbRush.Enabled = false;
            this.cbPrioritaire.CheckState = CheckState.Unchecked;
            this.cbPrioritaire.Enabled = false;
            this.tbItineraire.Text = "";
            this.tbItineraire.Enabled = false;
            this.tbClBagage.Text = "";
            this.tbClBagage.Enabled = false;
            this.btSave.Enabled = false;
            this.btSave.Visible = false;
        }

        private void Deconnecter()
        {
            this.groupBox1.Visible = this.groupBox2.Visible = this.groupBox3.Visible = false;
            toolStripStatusLabel2.Text = this.state.ToString();
        }

        private void ReinitialiserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OnPimStateChanged(PimState.AttenteBagage);
            this.toolStripStatusLabel1.Text = "";
        }
    }
}
