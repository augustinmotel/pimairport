﻿using Client.FormIhm.ServiceReferencePim;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client.FormIhm
{
    public partial class Select : Form
    {
        public BagageDefinition bagageResult;
        public Select()
        {
            InitializeComponent();
        }

        /*
         * Fonction de récupération du code sélectionné dans la popup
         */
        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            bagageResult = listIata.SelectedItem as BagageDefinition;
        }

        /*
         * Fonction de remplissage de la listbox contenant les codes iatas
         */
        public void FillList(List<BagageDefinition> list)
        {
            List<BagageDefinition> data = new List<BagageDefinition>();
            
            foreach (BagageDefinition bag in list)
            {
                data.Add(bag);
            }
            listIata.DisplayMember = "CodeIata";
            listIata.DataSource = data;

        }

        public void CloseForm(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
