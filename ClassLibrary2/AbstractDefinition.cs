﻿using MyAirport.Pim.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAirport.Pim.Models
{
    public abstract class AbstractDefinition
    {
        public abstract BagageDefinition GetBagage(int idBagage);
        public abstract List<BagageDefinition> GetBagage(string codeIataBagage);

        public abstract int InsertBagage(BagageDefinition bag);

        public abstract bool CheckCompagnie(String iata);
        public abstract bool CheckIataExist(String iata);

        public abstract bool CheckIataLength(String iata);
    }

}
