﻿using System.Collections.Generic;
using System.ServiceModel;
using MyAirport.Pim.Entities;

namespace MyAirport.Pim.Service
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "Service1" à la fois dans le code et le fichier de configuration.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class ServicePim : IServicePim
    {
        int NbAppelTotale=0;
        int NbAppelInstance=0;

        public bool CheckCompagnie(string iata)
        {
            bool exist = MyAirport.Pim.Models.Factory.Model.CheckCompagnie(iata);
            if(!exist)
            {
                CompagnieFormatException excp = new CompagnieFormatException();
                excp.Message = "Erreur : La compagnie n'existe pas";
                excp.Compagnie = iata;
                throw new FaultException<CompagnieFormatException>(excp);
            }
            return exist;
        }

        public bool CheckIataExist(string iata)
        {
            bool exist = MyAirport.Pim.Models.Factory.Model.CheckIataExist(iata);
            if(!exist)
            {
                IataExistException excp = new IataExistException();
                excp.CodeIata = iata;
                excp.Message = "Erreur : Le code iata existe deja";
                throw new FaultException<IataExistException>(excp);
            }
            return exist;
        }

        public bool CheckIataLength(string iata)
        {
            bool exist = MyAirport.Pim.Models.Factory.Model.CheckIataLength(iata);
            if (!exist)
            {
                IataFormatException excp = new IataFormatException();
                excp.CodeIata = iata;
                excp.Message = "Erreur : 6 caractères requis";
                throw new FaultException<IataFormatException>(excp);
            }
            return exist;
        }

        public int CreateBagage(BagageDefinition bag)
        {
            return Models.Factory.Model.InsertBagage(bag);
        }
        
        public BagageDefinition GetBagageByCodeIata(string codeIata)
        {
            NbAppelTotale++;
            this.NbAppelInstance++;
            List<BagageDefinition> res = MyAirport.Pim.Models.Factory.Model.GetBagage(codeIata);
            if (res != null && res.Count>0)
            {
                if (res.Count == 1)
                {
                    return res[0];
                }
                else
                {
                    var excp = new MultipleBagagesFault();
                    excp.CodeIata = codeIata;
                    excp.Message = "Trop bagages en base";
                    excp.ListBagages = res;
                    throw new FaultException<MultipleBagagesFault>(excp);
                }
            }
            else
            {
                return null;
            }

        }

        public BagageDefinition GetBagageById(int idBagage)
        {
            return MyAirport.Pim.Models.Factory.Model.GetBagage(idBagage);
        }

    }
}
