﻿using MyAirport.Pim.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MyAirport.Pim.Service
{
    [ServiceContract]
    public interface IServicePim
    {
        [OperationContract]
        BagageDefinition GetBagageById(int idBagage);
        [OperationContract]
        [FaultContract(typeof(MultipleBagagesFault))]
        BagageDefinition GetBagageByCodeIata(string codeIata);
        [OperationContract]
        int CreateBagage(BagageDefinition bag);
        [OperationContract]
        [FaultContract(typeof(CompagnieFormatException))]
        bool CheckCompagnie(String iata);
        [OperationContract]
        [FaultContract(typeof(IataExistException))]
        bool CheckIataExist(String iata);
        [OperationContract]
        [FaultContract(typeof(IataFormatException))]
        bool CheckIataLength(String iata);
    }
}
