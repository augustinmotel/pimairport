﻿using System;
using System.Collections.Generic;
using System.Configuration;
using MyAirport.Pim.Entities;
using System.Data.SqlClient;

namespace MyAirport.Pim.Models
{
    public class Sql : AbstractDefinition
    {
        private string strCnx = ConfigurationManager.ConnectionStrings["MyAirport.Pim.Settings.DbConnect"].ConnectionString;
        private string commandGetBagageId =
            "SELECT b.ID_BAGAGE, c.NOM as compagnie, b.CODE_IATA, b.LIGNE, b.DATE_CREATION, b.ESCALE, b.PRIORITAIRE ," +
            "cast(iif(b.CONTINUATION='N',0,1) as bit) as Continuation, cast(iif(bp.PARTICULARITE is null, 0, 1) as bit) as 'RUSH' " +
            "FROM BAGAGE b " +
            "LEFT OUTER JOIN BAGAGE_A_POUR_PARTICULARITE bap on bap.ID_BAGAGE = b.ID_BAGAGE " +
            "LEFT OUTER JOIN BAGAGE_PARTICULARITE bp on bp.ID_PART = bap.ID_PARTICULARITE and bp.PARTICULARITE = 'RUSH', b.CLASSE " +
            "LEFT OUTER JOIN COMPAGNIE c on c.CODE_IATA = b.COMPAGNIE " +
            "INNER JOIN COMPAGNIE_CLASSE cc on cc.ID_COMPAGNIE = c.ID_COMPAGNIE and cc.CLASSE = b.CLASSE " +
            "WHERE b.id_bagage = @id";

        private string commandGetBagageIata =
            "SELECT b.ID_BAGAGE, c.NOM as compagnie, b.CODE_IATA, b.LIGNE, b.DATE_CREATION, b.ESCALE, b.PRIORITAIRE ," +
            "cast(iif(b.CONTINUATION='N',0,1) as bit) as Continuation, cast(iif(bp.PARTICULARITE is null, 0, 1) as bit) as 'RUSH', b.CLASSE " +
            "FROM BAGAGE b " +
            "LEFT OUTER JOIN BAGAGE_A_POUR_PARTICULARITE bap on bap.ID_BAGAGE = b.ID_BAGAGE " +
            "LEFT OUTER JOIN BAGAGE_PARTICULARITE bp on bp.ID_PART = bap.ID_PARTICULARITE and bp.PARTICULARITE = 'RUSH' " +
            "LEFT OUTER JOIN COMPAGNIE c on c.CODE_IATA = b.COMPAGNIE " +
            "LEFT OUTER JOIN COMPAGNIE_CLASSE cc on cc.ID_COMPAGNIE = c.ID_COMPAGNIE and cc.CLASSE = b.CLASSE " +
            "WHERE b.CODE_IATA LIKE @code";

        private string commandInsertBagage =
            "INSERT INTO BAGAGE(CODE_IATA,COMPAGNIE,LIGNE,CLASSE,PRIORITAIRE,CONTINUATION,JOUR_EXPLOITATION,ESCALE,ORIGINE_CREATION,DATE_CREATION) " +
            "VALUES(@iata, @compagnie, @ligne, @classe, @prioritaire, @continuation, '1', @itineraire,'D', GETDATE());" +
            "DECLARE @bag as int = SCOPE_IDENTITY();" +
            "INSERT INTO BAGAGE_A_POUR_PARTICULARITE(ID_BAGAGE, ID_PARTICULARITE) " +
            "VALUES(@bag, @particularite);";

        private string commandGetCompagnie = "SELECT CODE_IATA FROM COMPAGNIE WHERE CODE_IATA = @iata";

        private string commandGetIata = "SELECT CODE_IATA FROM BAGAGE WHERE CODE_IATA = @iata";

        public override Entities.BagageDefinition GetBagage(int idBagage)
        {
            BagageDefinition bagRes = null;
            using (SqlConnection cnx = new SqlConnection(strCnx))
            {
                SqlCommand cmd = new SqlCommand(this.commandGetBagageId, cnx);
                cmd.Parameters.AddWithValue("@id", idBagage);
                cnx.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    if (sdr.Read())
                    {
                        bagRes = new BagageDefinition
                        {
                            CodeIata = sdr.GetString(sdr.GetOrdinal("code_iata")),
                            Compagnie = sdr.GetString(sdr.GetOrdinal("compagnie")),
                            DateVol = sdr.GetDateTime(sdr.GetOrdinal("date_creation")),
                            EnContinuation = sdr.GetBoolean(sdr.GetOrdinal("continuation")),
                            IdBagage = sdr.GetInt32(sdr.GetOrdinal("id_bagage")),
                            Itineraire = sdr.GetString(sdr.GetOrdinal("escale")),
                            Ligne = sdr.GetString(sdr.GetOrdinal("ligne")),
                            Prioritaire = sdr.GetBoolean(sdr.GetOrdinal("prioritaire")),
                            ClasseBagage = sdr.GetString(sdr.GetOrdinal("classe"))
                        };
                    }
                }
            }

            return bagRes;
        }
        
        public override List<BagageDefinition> GetBagage(string codeIataBagage)
        {
            List<BagageDefinition> bagsRes = new List<BagageDefinition>();
            using (SqlConnection cnx = new SqlConnection(strCnx))
            {
                SqlCommand cmd = new SqlCommand(this.commandGetBagageIata, cnx);
                cmd.Parameters.AddWithValue("@code", "%"+codeIataBagage+"%");
                cnx.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        BagageDefinition bagRes = new BagageDefinition
                        {
                            CodeIata = sdr.GetString(sdr.GetOrdinal("code_iata")),
                            Compagnie = sdr.GetString(sdr.GetOrdinal("compagnie")),
                            DateVol = sdr.GetDateTime(sdr.GetOrdinal("date_creation")),
                            EnContinuation = sdr.GetBoolean(sdr.GetOrdinal("continuation")),
                            IdBagage = sdr.GetInt32(sdr.GetOrdinal("id_bagage")),
                            Itineraire = sdr.GetString(sdr.GetOrdinal("escale")),
                            Ligne = sdr.GetString(sdr.GetOrdinal("ligne"))
                        };
                        if (!(sdr["prioritaire"].Equals(System.DBNull.Value)))
                        {
                            bagRes.Prioritaire = sdr.GetBoolean(sdr.GetOrdinal("prioritaire"));
                        }
                        else
                        { 
                            bagRes.Prioritaire = false;
                        }
                        if (!(sdr["classe"].Equals(System.DBNull.Value)))
                        {
                            bagRes.ClasseBagage = sdr.GetString(sdr.GetOrdinal("classe"));
                        }
                        else
                        {
                            bagRes.ClasseBagage = "Y";
                        }
                        if (sdr.GetBoolean(sdr.GetOrdinal("rush")))
                        {
                            bagRes.Rush = true;
                        }
                        else
                        {
                            bagRes.Rush = false;
                        }
                        bagsRes.Add(bagRes);
                    
                    }
                }
                return bagsRes;
            }
        }
        
        public override int InsertBagage(BagageDefinition bag)
        {
            int value;
            using (SqlConnection cnx = new SqlConnection(strCnx))
            {
                SqlCommand cmd = new SqlCommand(this.commandInsertBagage, cnx);
                cmd.Parameters.AddWithValue("@iata","0000"+bag.CodeIata+"00");
                cmd.Parameters.AddWithValue("@compagnie", bag.Compagnie);
                cmd.Parameters.AddWithValue("@ligne", bag.Ligne);
                cmd.Parameters.AddWithValue("@classe", bag.ClasseBagage);
                if (bag.Prioritaire)
                {
                    cmd.Parameters.AddWithValue("@prioritaire", 1);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@prioritaire", 0);
                }
                if (bag.EnContinuation)
                {
                    cmd.Parameters.AddWithValue("@continuation", "Y");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@continuation", "N");
                }
                cmd.Parameters.AddWithValue("@itineraire", bag.Itineraire);
                if (bag.Rush)
                {
                    cmd.Parameters.AddWithValue("@particularite", 15);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@particularite", 1000100110);
                }
                cnx.Open();
                value=cmd.ExecuteNonQuery();
            }
            return value;
        }

        /*
         * Vérifie si la compagnie spécifiée existe afin de ne pas insérer d'erreur dans la base
         */
        public override bool CheckCompagnie(String iata)
        {
            bool exist = false;
            using (SqlConnection cnx = new SqlConnection(strCnx))
            {
                SqlCommand cmd = new SqlCommand(this.commandGetCompagnie, cnx);
                cmd.Parameters.AddWithValue("@iata",iata);
                cnx.Open();
                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    if (dataReader.HasRows)
                    {
                        exist = true;
                    }
                }
            }
            return exist;
        }

        public override bool CheckIataExist(String iata)
        {
            bool exist = true;
            using (SqlConnection cnx = new SqlConnection(strCnx))
            {
                SqlCommand cmd = new SqlCommand(this.commandGetIata, cnx);
                cmd.Parameters.AddWithValue("@iata", "0000"+iata+"00");
                cnx.Open();
                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    if (dataReader.HasRows)
                    {
                        exist = false;
                    }
                }
            }            
            return exist;
        }

        public override bool CheckIataLength(String iata)
        {
            bool exist = true;
            if(iata.Length != 6)
            {
                exist = false;
            }
            return exist;
        }
    }
}
